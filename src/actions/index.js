import transformForecast from "./../services/transformForecast";
import getUrlWeatherByCity from "./../services/getUrlWeatherByCity.js";
import transformWeather from "./../services/transformWeather";
export const SET_CITY = "SET_CITY";
export const SET_FORECAST_DATA = "SET_FORECAST_DATA";
export const GET_WEATHER_CITY = "GET_WEATHER_CITY";
export const SET_WEATHER_CITY = "SET_WEATHER_CITY";

const setCity = payload => ({ type: SET_CITY, payload });
const setForecastData = payload => ({
    type: SET_FORECAST_DATA,
    payload
});

const getWeatherCity = payload => ({ type: GET_WEATHER_CITY, payload });
const setWeatherCity = payload => ({ type: SET_WEATHER_CITY, payload });
const api_key = "27f37387d971e0b9043a51de6f61312d";
const url = "http://api.openweathermap.org/data/2.5/forecast";

export const setSelectedCity = payload => {
    return (dispatch, getState) => {
        const url_forecast = `${url}?q=${payload}&appid=${api_key}`;

        dispatch(setCity(payload));
        const state = getState();
        const date =
            state.city[payload] && state.city[payload].forecastDataDate;
        const now = new Date();
        if (date && now - date < 1 * 60 * 1000) {
            return;
        }
        return fetch(url_forecast)
            .then(data => data.json())
            .then(weather_data => {
                const forecastData = transformForecast(weather_data);
                console.log(payload);
                dispatch(setForecastData({ city: payload, forecastData }));
            });
    };
};

export const setWeather = payload => {
    return dispatch => {
        payload.forEach(city => {
            dispatch(getWeatherCity(city));
            const api_weather = getUrlWeatherByCity(city);
            fetch(api_weather)
                .then(resolve => {
                    return resolve.json();
                })
                .then(data => {
                    const weather = transformWeather(data);
                    dispatch(setWeatherCity({ city, weather }));
                });
        });
    };
};
