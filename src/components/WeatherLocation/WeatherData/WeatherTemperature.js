import React from "react";
import PropTypes from "prop-types";
import WeatherIcons from "react-weathericons";
import "./styles.css";
import {
    CLOUD,
    SUN,
    FOG,
    RAIN,
    SNOW,
    THUNDER,
    DRIZZLE
} from "./../../../constants/weathers";
const icons = {
    [CLOUD]: "cloud",
    [SUN]: "day-sunny",
    [FOG]: "day-fog",
    [RAIN]: "rain",
    [SNOW]: "snow",
    [THUNDER]: "day-thunderstorm",
    [DRIZZLE]: "day-showers"
};

const getWeatherIcon = weatherState => {
    const icon = icons[weatherState];
    const sizeIcon = "4x";
    if (icon)
        return <WeatherIcons className="wicon" name={icon} size={sizeIcon} />;
    else
        return (
            <WeatherIcons
                className="wicon"
                name={"day-sunny"}
                size={sizeIcon}
            />
        );
};

const WeatherTemperature = props => {
    const { temperature, weatherState } = props;
    return (
        <div className="weatherTemperatureCont">
            {getWeatherIcon(weatherState)}
            <span className="temperature">{`${temperature}`}</span>
            <span className="temperatureType">{` °C`}</span>
        </div>
    );
};
//Validacion para las propiedades
WeatherTemperature.propTypes = {
    temperature: PropTypes.number.isRequired,
    weatherState: PropTypes.string
};

export default WeatherTemperature;
