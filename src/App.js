import React, { Component } from "react";
import { Grid, Row, Col } from "react-flexbox-grid";
import Paper from "@material-ui/core/Paper";
import AppBar from "@material-ui/core/AppBar";
import Typography from "@material-ui/core/Typography";
import Toolbar from "@material-ui/core/Toolbar";
import "./App.css";
import LocationListContainer from "./containers/LocationListContainer";
import ForeCastExtendedContainer from "./containers/ForeCastExtendedContainer";
const cities = ["Mexico,mx", "Washington,us", "Bogota,col", "Madrid,es"];

class App extends Component {
    render() {
        return (
            <Grid>
                <Row>
                    <AppBar position="sticky">
                        <Toolbar>
                            <Typography variant="title" color="inherit">
                                App
                            </Typography>
                        </Toolbar>
                    </AppBar>
                </Row>
                <Row>
                    <Col xs={12} md={6}>
                        <div className="App">
                            <LocationListContainer cities={cities} />
                        </div>
                    </Col>
                    <Col xs={12} md={6}>
                        <Paper elevation={4}>
                            <div className="details">
                                <ForeCastExtendedContainer />
                            </div>
                        </Paper>
                    </Col>
                </Row>
            </Grid>
        );
    }
}
export default App;
