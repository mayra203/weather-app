export const CLOUD = "cloud";
export const SUN = "sun";
export const FOG = "fog";
export const RAIN = "rain";
export const SNOW = "snow";
export const THUNDER = "thunder";
export const DRIZZLE = "drizzle";
